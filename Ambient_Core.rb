live_loop :ambient do
  play 36
  play 48
  sample :ambi_lunar_land, amp: 0.8
  sample :ambi_drone, amp: 0.7
  sleep 4
end

live_loop :beats do
  sample :bd_ada, amp: 2.5
  sleep 0.25
end

live_loop :bass do
  sample :bd_haus, amp: 1.5
  sleep 0.5
end

live_loop :bass_snapp do
  sleep 1.75
  sample :bd_haus, amp: 1.5
  sleep 0.25
end

live_loop :hit do
  sample :bass_hit_c, amp: 2
  sleep 0.75
  sample :bass_hit_c, amp: 0.75
  sleep 0.75
end

with_synth  :dpulse do
  live_loop :melody do
    4.times do
      play_pattern_timed chord(:E3, :m7), 0.25
    end
    4.times do
      play_pattern_timed chord(:C3, :m7), 0.25
    end
    4.times do
      play_pattern_timed chord(:F3, :m7), 0.25
    end
    4.times do
      play_pattern_timed chord(:G3, :m7), 0.25
    end
  end
end

