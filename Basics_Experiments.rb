# Welcome to Sonic Pi

live_loop :medley do
  sleep 1
end

define :pattern do |pattern|
  return pattern.ring.tick == "x"
end

cmaster1 = 115

live_loop :base, sync: :medley do
  sample :bd_sone, cutoff: cmaster1 if pattern "x-xx--x---x--x--"
  sleep 0.25
end

live_loop :that_alien, sync: :medley do
  sample :ambi_lunar_land, cutoff: cmaster1, amp: 1.2
  sleep 8
end

live_loop :ticki, sync: :medley do
  sample :elec_blip, cutoff: cmaster1, amp: 0.8 if pattern "x-xx-x-x-xx-x--x"
  sleep 0.125
end

with_synth :dpulse do
  live_loop :melody, sync: :medley do
    use_synth :tri
    4.times do
      play_pattern_timed chord(:E3, :m7), 0.25
    end
    4.times do
      play_pattern_timed chord(:C3, :m7), 0.25
    end
    4.times do
      play_pattern_timed chord(:F3, :m7), 0.25
    end
    4.times do
      play_pattern_timed chord(:G3, :m7), 0.25
    end
  end
end