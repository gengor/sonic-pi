# Welcome to Sonic Pi v3.1

live_loop :ambi do
  sample :ambi_drone, pitch: rrand(0.5, 1)
  sleep 2
end

live_loop :bass do
  sample :bd_klub
  sleep 0.5
  sample :bd_gas
  sleep 0.5
end

notes1 = (ring 55, 59, 62, 65)
notes2 = (ring 58, 62, 65, 68)
notes3 = (ring 75, 72, 68, 65)
live_loop :chill do
  use_synth :dpulse
  play notes2.tick, release: 1, amp: rrand(0.4, 0.9)
  sleep 0.25
end

live_loop :beat do
  14.times do
    sample :drum_cymbal_soft, amp: 0.7
    sleep 0.25
  end
  4.times do
    sample :drum_cymbal_soft, amp: 0.5
    sleep 0.125
  end
end
