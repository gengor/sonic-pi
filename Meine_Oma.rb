live_loop :melody do
  use_synth :pluck
  use_bpm 120
  play 60
  sleep 1
  play 62
  sleep 1
  play 64
  sleep 1
  play 64
  sleep 1
  play 62
  sleep 1
  play 62
  sleep 1
  play 60, release: 1.5
  sleep 2
  
  2.times do
    play 69
    sleep 1
    play 72
    sleep 1
    play 71
    sleep 1
    play 69
    sleep 1
    play 67, release: 1.5
    sleep 2
    play 64, release: 1.5
    sleep 2
  end
  
  play 60
  sleep 1
  play 62
  sleep 1
  play 64
  sleep 1
  play 62
  sleep 1
  play 60, release: 1.5
  sleep 2
  play 60, release: 1.5
  sleep 2
  
end

live_loop :beats do
  #sample :bd_haus, amp: 1
  sleep 1
end

live_loop :drums do
  #sample :loop_safari, amp: 2
  sample :loop_amen, beat_stretch: 2
  sleep 2
end

