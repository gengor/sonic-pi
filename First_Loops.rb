## Martin Palmowski, my first steps with Sonic Pi!
p1 = [
  chord(:C3,"major"),
  chord(:F3,"major"),
  chord(:D3,"major"),
  chord(:F4,"major"),
  chord(:E3,"major"),
  chord(:D3,"major"),
  chord(:G3,"major"),
  chord(:A4,"major")
]

cc = chord(:C3, "major")

with_fx :panslicer do
  with_fx :reverb do
    with_synth :piano do
      live_loop :chords do
        p1.each {
          |c|
          cc = c
          play c, decay: 1.5
          play c+12, decay: 1.5, amp: 0.5
          sleep 1
          play c+24, decay: 1.5, amp: 0.5
          sleep 1
        }
      end
    end
  end
end


##| live_loop :choir do
##|   sample :ambi_choir
##|   sleep 1
##|   sample :ambi_choir, rate: 1.2
##|   sleep 1
##|   sample :ambi_choir, rate: 0.8
##|   sleep 1
##|   sample :ambi_choir, rate: 1.1
##|   sleep 1
##| end

live_loop :base do
  sample :bd_haus
  sleep 1.75
  sample :bd_haus, amp: 1.5
  sleep 0.25
  sample :bd_haus
  sleep 2
end

with_fx :bitcrusher do
  live_loop :tick do
    7.times do
      sample :drum_cymbal_soft, amp: 0.40
      sleep 0.25
    end
    
    2.times do
      sample :drum_cymbal_soft, amp: 0.40
      sleep 0.125
    end
    
  end
end

with_fx :level, amp: 0.6 do
  live_loop :random_melody do
    ##| vs = [1,0,1,0 ,0,0,1,1 ,0,1,0,1].choose
    ##| play rrand(60, 70), amp: vs*0.5, attack: 0.25, release: 0
    a = [0,0,1,2,2,2,2].choose
    case a
    when 0
      with_synth :piano do
        play_pattern_timed scale(:c2, :major), 0.125, release: 0.1
      end
      sleep 0.5
    when 1
      with_synth :prophet do
        play_pattern_timed scale(:e2, :major_pentatonic, num_octaves: 2), 0.125, release: 0.1
      end
      
      sleep 0.5
    when 2
      ##| play_pattern_timed scale(:e1, :major, num_octaves: 2), 0.125, release: 0.1
      sleep 0.5
    end
  end
end


live_loop :amen_break do
  sample :loop_amen, beat_stretch: 2
  sleep 2
end