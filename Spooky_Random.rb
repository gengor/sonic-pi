with_fx :reverb, room: 1 do
  use_synth :pluck
  
  live_loop :bells do
    play rrand_i(60, 72), amp: rrand(0.1, 0.5), release: rrand(0.4, 2)
    sleep 0.25
  end
end

live_loop :ticks do
  sleep 8
  32.times do
    sample :bass_hit_c, amp: rrand(0.2, 0.5)
    sleep 0.25
  end
end

live_loop :beat do
  sample :bd_ada
  sleep 1.75
  sample :bd_ada
  sleep 0.25
  sample :bd_ada
  sleep 2
end

with_fx :flanger do
  live_loop :ambient do
    sleep 0.25
    sample :ambi_dark_woosh, amp: 0.2
    sleep 3.75
  end
end

with_fx :reverb, room: 1, amp: 1 do
  live_loop :space do
    sample :ambi_lunar_land, amp: 0.3
    sleep 10
  end
end

with_fx :echo, decay: 5, max_phase: 2 do
  live_loop :melody do
    use_synth :piano
    play_pattern chord(:c3, :minor, num_octaves: 2), 0.125, release: 2, amp: 0.8
    sleep 2
    play_pattern chord(:fs3, :minor, num_octaves: 2), 0.125, release: 2, amp: 0.8
    sleep 2
  end
end